"use strict";

module.exports = {
    up: function (migration, DataTypes, done) {
        // add altering commands here, calling 'done' when finished
        migration.addColumn(
            'Products',
            'isMoney', {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }
        );
        done();
    },

    down: function (migration, DataTypes, done) {
        migration.removeColumn('Products','isMoney');
        // add reverting commands here, calling 'done' when finished
        done();
    }
};
