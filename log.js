var logger = require('winston');
var path = require('path');
var config = require('./config/config.json');

logger.setLevels({
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
});
logger.addColors({
    debug: 'green',
    info: 'cyan',
    warn: 'yellow',
    error: 'red'
});
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',
    handleExceptions: true,
    prettyPrint: true,
    silent: false,
    timestamp: true,
    colorize: true,
    json: true
});
logger.add(logger.transports.DailyRotateFile,
    {
        level: 'info',
        name: 'log#debug',
        filename: path.join(__dirname, config.log_folder, "log_file.log")
    });
module.exports = logger;
