var express = require('express');
var fbManager = require('../utils/facebookManager');
var RSVP = require('rsvp');
var logger = require('winston');
var _ = require('lodash');
var path = require('path');
var fs = require('fs');
var router = express.Router();

router.param('fb_token', function (req, res, next, fb_token) {
    if (!fb_token) {
        next(new Error('Token not present'));
    } else {
        req.fb_token = fb_token;
        // go to the next thing
        next();
    }
});

// GET /user/fb/:fb_token
router.get('/fb/:fb_token', function (req, res) {
    logger.info('Register requested for token: ' + req.fb_token);
    var User = req.app.get('models').User;
    var tasks = {
        verify: fbManager.verifyToken(req.fb_token),
        user: fbManager.getUser(req.fb_token)
    };

    RSVP.hash(tasks).then(function (results) {
        logger.info(module.filename + '::Tasks are finished' + results.user.name);
        if (!results.verify.is_valid || !results.user.verified) {
            res.status(203).json({
                is_valid: false
            });
        } else {
            User.findOrCreate({
                where: {
                    email: results.user.email
                },
                defaults: {
                    photo: fbManager.buildUserPhotoUrl(results.user.id),
                    fb_id: results.user.id,
                    name: results.user.name,
                    email: results.user.email,
                    fb_token: req.fb_token
                }
            }).spread(function (user, created) {
                logger.info(module.filename + '::Search result returned user: ' + user);
                res.status(200).json({
                    status: true,
                    name: user.values.name,
                    email: user.values.email,
                    photo: user.values.photo,
                    user_id: user.id
                });
            }).catch(function (err) {
                logger.info(module.filename + '::Error finding user: ' + err);
                res.status(400).json(err);
            });
        }
    }).catch(function (err) {
        logger.error(module.filename + 'Huston we have a problem: ' + err);
        res.status(500).json(err);
    });

});

// PUT /user/:user_id -> {name: string, email: string, token: UUID, lot: decimal, lat: decimal }
router.put('/:user_id', function (request, response) {
    request.app.get('models').User
        .find(request.params.user_id)
        .success(function (user) {
            user.updateAttributes(_.omit(request.body, 'id', 'fb_id', 'fb_token', 'email', 'name', 'createdAt', 'updatedAt'))
                .success(function () {
                    response.sendStatus(200);
                })
                .error(function (err) {
                    response.status(400).json(err);
                });
        })
        .error(function (err) {
            response.status(400).json(err);
        });
});

router.get('/:user_id', function (request, response) {
    var db = request.app.get('models');
    db.User
        .find({
            where: {
                id: request.params.user_id
            },
            include: [{model: db.Product, order: [ [ db.Product, 'createdAt', 'DESC' ] ], include: [db.ProductPhoto]}]
        })
        .then(function (user) {
            response.status(200).json(user);
        })
        .catch(function (err) {
            response.status(400).json(err);
        });
});


router.get('/:user_id/matches', function (request, response) {
    var db = request.app.get('models');
   var queryString = ''+'select* from (select other_product_info_tmp1.*,Users.name as source_user_name from (select other_product_info_tmp.*,'+ 
   'product_photos_other_product.path as product_path_target FROM'+
   '(select other_Likes.user_id as user_id_source, other_Likes.liked_product_id as product_id_target,' +
   'other_Products.user_id as user_id_target, other_Products.name as product_name_target,'+
   ' other_Products.price as product_price_target, other_Products.supplement as product_supplement_target,'+
   ' other_Products.isMoney as product_isMoney_target from Likes as other_Likes join Products as other_Products'+
   ' on other_Likes.liked_product_id = other_Products.id where other_Likes.user_id=:user_id'+
   ' and other_Likes.disliked is not true)as other_product_info_tmp LEFT JOIN(select * from '+
   'ProductPhotos GROUP BY ProductPhotos.product_id) as product_photos_other_product ON '+
   'product_photos_other_product.product_id = other_product_info_tmp.product_id_target)as other_product_info_tmp1 JOIN Users on other_product_info_tmp1.user_id_source=Users.id) as other_product_info join'+
   ' (select current_product_info_tmp1.*,Users.name as target_user_name from( select current_product_info_tmp.*, product_photos_current_product.path as product_path_source from'+
   ' (select current_Likes.user_id as user_id_target_confirm, current_Likes.liked_product_id as product_id_source,'+
   ' current_Products.user_id as user_id_source_confirm, current_Products.name as product_name_source,'+
   ' current_Products.price as product_price_source, current_Products.supplement as product_supplement_source,'+
   ' current_Products.isMoney as product_isMoney_source from Likes as current_Likes join Products as current_Products'+
   ' on current_Likes.liked_product_id = current_Products.id where current_Products.user_id = :user_id'+
   ' and current_Likes.disliked is not true) as current_product_info_tmp LEFT JOIN '+
   '(select * from ProductPhotos group by ProductPhotos.product_id) as product_photos_current_product'+
   ' on product_photos_current_product.product_id=current_product_info_tmp.product_id_source)as current_product_info_tmp1 JOIN Users on current_product_info_tmp1.user_id_target_confirm=Users.id) as '+
   'current_product_info on other_product_info.user_id_source=current_product_info.user_id_source_confirm'+
   ' and other_product_info.user_id_target=current_product_info.user_id_target_confirm where (product_price_target+product_supplement_target'+
   ' BETWEEN 0.8*(product_price_source+product_supplement_source) AND 1.2*(product_price_source+product_supplement_source)) OR '+
   '(product_price_source+product_supplement_source'+
   ' BETWEEN 0.8*(product_price_target+product_supplement_target) AND 1.2*(product_price_target+product_supplement_target))';

    db.sequelize.query(queryString, {
            type: db.sequelize.QueryTypes.SELECT,
            replacements: {
                user_id: request.params.user_id
            }
        })
        .then(function (matches) {
            response.status(200).json(matches);
        })
        .catch(function (err) {
            response.status(400).json(err);
        });
});

router.delete('/:user_id', function (request, response) {
    var db = request.app.get('models');
    db.User
        .find({
            where: {
                id: request.params.user_id
            },
            include: [{model: db.Product, include: [db.ProductPhoto]}]
        })
        .then(function (user) {
            if (!user) {
                throw "User not found";
            }
            user.Products.forEach(function(product){
                product.ProductPhotos.forEach(function (photo) {
                    try {
                        fs.unlinkSync(path.join(__dirname, '../', photo.path));
                    } catch (e) {
                        // file not found. move on
                    }
                });
            });
            return user.destroy();
        })
        .then(function (user) {
            response.status(204).json(user);
        })
        .catch(function (err) {
            response.status(400).json(err);
        });

});


// GET /user/:user_id/suggestions|?radius=40&priceOffset=10|
router.get('/:user_id/suggestions', function (req, res) {
    var queryString = 'SELECT Products.*, Photos.path, ' +
        '(((acos(sin(( :latitude *pi()/180)) * sin((`lat`*pi()/180))+cos(( :latitude *pi()/180)) * cos((`lat`*pi()/180)) * cos((( :longitude  - `lon`)* pi()/180))))*180/pi())*60*1.1515*1.609344 ) ' +
        'AS distance ' +
        'FROM `Users` JOIN `Products` on `Users`.id = Products.user_id ' +
        'LEFT JOIN ( ' +
        ' SELECT DISTINCT product_id, path FROM ProductPhotos GROUP BY product_id ) AS Photos ' +
        'on Photos.product_id = Products.id ' +
        'WHERE :user_id != Products.user_id ' +
        'AND Products.id NOT  IN (select liked_product_id from Likes where Likes.user_id = :user_id) ' +
//         'AND Products.price+Products.supplement BETWEEN :lowest_price AND :highest_price ' +
        'HAVING distance <= :radius ';

    var db = req.app.get('models');

    db.User.find(req.params.user_id)
        .then(function (user) {
            if ( !user.lat || !user.lon ) {
                throw "No user location data available.";
            }
            return db.sequelize.query(queryString, {
                type: db.sequelize.QueryTypes.SELECT,
                replacements: {
                    latitude: user.lat,
                    longitude: user.lon,
                    user_id: user.id,
                    radius: req.query.radius || 50
                }
            })
        })
        .then(function (productsWithDistance) {
            res.status(200).json(productsWithDistance);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
        
  
});

router.get('/:user_id/chats', function (request, response) {
    var db = request.app.get('models');
    var query = 'SELECT Chats.product_id, Chats.participant_product_id, Chats.message, Users.name as user_name, Users.id as user_id, participant_users.id as paritcipant_user_id , participant_users.name as paritcipant_user_name , Chats.createdAt as createdAt ' +
    'FROM Chats join Products on Products.id = Chats.product_id join Users on Users.id = Products.user_id '+
        'JOIN (SELECT Users.*, Products.id as product_id FROM Products JOIN Users ON Users.id = Products.user_id) AS participant_users ON participant_users.product_id = Chats.participant_product_id '+
        'WHERE (Users.id = :user_id OR participant_users.id = :user_id) AND Chats.createdAt > STR_TO_DATE( :since, \'%Y-%m-%d %H:%i:%s\')';
    var since_date = request.query.since || '2014-10-15 10:00:00';

    db.sequelize.query(query, {
        type: db.sequelize.QueryTypes.SELECT,
        replacements: {
            user_id: request.params.user_id,
            since: since_date
        }
    })
    .then(function (matches) {
        response.status(200).json(matches);
    })
    .catch(function (err) {
        response.status(400).json(err);
    });
});
module.exports = router;
