var express = require('express');
var logger = require('winston');
var _ = require('lodash');
var pushManager = require('./../utils/iosPushManager');
var router = express.Router();





router.get('/', function (request, response) {
    var Chat = request.app.get('models').Chat;
    Chat.findAll({
            where: {
                $or: [{
                    product_id: request.productParams.product_id,
            }, {
                    participant_product_id: request.productParams.product_id
            }]
            },
            include: [request.app.get('models').Product]
        })
        .then(function (results) {
            response.status(200).json(results);
        }).catch(function (error) {
            response.status(400).json(error);
        });
});

router.get('/:participant_product_id', function (request, response) {
    var Chat = request.app.get('models').Chat;
    var sinceDate = isNaN(+request.query.since) ? Date.parse(request.query.since) : +request.query.since;
    Chat.findAll({
            where: {
                $or: [{
                    product_id: request.productParams.product_id,
                    participant_product_id: request.params.participant_product_id
            }, {
                    product_id: request.params.participant_product_id,
                    participant_product_id: request.productParams.product_id
            }],
                createdAt: {
                    $gte: new Date(Date.parse(sinceDate || 0))
                }
            },
            order: 'createdAt ' + ( request.query.orderDate || 'DESC' )
        })
        .then(function (results) {
            response.status(200).json(results);
        }).catch(function (error) {
            response.status(400).json(error);
        });
});

router.post('/:participant_product_id', function (request, response) {
    var participant_product_id = request.params.participant_product_id || request.body.participant_product_id;
    if (participant_product_id === request.productParams.product_id) {
        response.status(400).json('No can do this. Send message to yourself?');
        return;
    }
    if (!request.body.message) {
        response.status(400).json('No can do this. What for you send empty message?');
        return;
    }
    var participant, sender;
    var db = request.app.get('models');
    var options = {
        product_id: request.productParams.product_id,
        participant_product_id: participant_product_id,
        message: request.body.message
    };
    db.Product.find({
        where: {
            id: request.productParams.product_id
        },
        include: [db.User]
    })
        .then(function (sender_product){
            sender = sender_product.User;
            return db.Product.find({
                where: {
                    id: options.participant_product_id
                },
                include: [db.User]
            });
        })
        .then(function (participant_product) {
            if (!participant_product) {
                response.status(400).json('Non existing product');
                return;
            }
            participant = participant_product.User || participant_product.user;
            return db.Chat.create(options);
        })
        .then(function (chatMessage) {
            if (participant.notify_chat === 'true' || participant.notify_chat === true) {
                pushManager.notifyUser(participant, sender.name+': '+options.message, {
                    product_id: options.participant_product_id,
                    participant_product_id: options.product_id,
                    message: options.message
                });
            }
            response.status(201).json(chatMessage);
        })
        .catch(function (error) {
            response.status(400).json(error);
        });
});

module.exports = router;
