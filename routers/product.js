var express = require('express');
var logger = require('winston');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
//var photosDir = './public/products/';
var photosDir = '/public/products/';

function filesToArray(filesObj) {
    var filesArray = [];
    var keys = _.keys(filesObj);
    _.forEach(keys, function (key) {
        if (_.isArray(filesObj[key])) {
            var fileNames = _.map(filesObj[key], 'name');
            filesArray = filesArray.concat(fileNames);
        } else {
            filesArray.push(filesObj[key]['name']);
        }
    });
    return filesArray;
}

function passProductParams(req, res, next) {
    req.productParams = req.params;
    next();
}

/*Configure the multer.*/
var multer_options = {
    dest: './public/products/',
    limits: {
        files: 4
    },
    putSingleFilesInArray: true,
    rename: function (fieldname, filename) {
        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now();
    },
    onFileUploadStart: function (file) {
        if (!~['image/png', 'image/jpeg'].indexOf(file.mimetype)) {
            logger.info('Bad file mime-type.Aborting upload. - ' + file.mimetype);
        }
        logger.info(file.originalname + ' upload is starting ...');
    },
    onFileUploadComplete: function (file) {
        logger.info(file.fieldname + ' uploaded to  ' + file.path);
    },
    onError: function (error, next) {
        logger.warn('Error uploading product image: ' + error);
        next(error);
    }
};

// POST /product -> {name: string, user_id: UUID, description: string, photos: stream, ends_at: DateTime, price: decimal, suplemment: decimal, isMoney: boolean}
router.route('/')
    .post(multer(multer_options), function (req, res) {
        var filesUploaded = filesToArray(req.files);
        filesUploaded = _.map(filesUploaded, function (fileName) {
            return photosDir + fileName;
        });
        var createdProduct;
        var db = req.app.get('models');
        var User = db.User;
        var productData = _.omit(req.body, 'id', 'createdAt', 'updatedAt');
        User
            .find(req.body.user_id)
            .then(function (user) {
                if (!user) {
                    throw 'No user available for this ID';
                }
                delete req.body.user_id;
                return user.createProduct(productData);
            })
            .then(function (product) {
                var promises = [];
                if ( product.isMoney === 'true' || eval(product.isMoney) ) {
                    res.status(200).json(product);
                    return;
                }
                filesUploaded.forEach(function (productPhoto) {
                    promises.push(product.createProductPhoto({
                        path: productPhoto
                    }));
                });
                createdProduct = product;
                return db.Sequelize.Promise.all(promises);
            })
            .then(function (productPhotos) {
                res.status(200).json(createdProduct);
            })
            .catch(function (err) {
                res.status(400).json(err);
            });
    });


// GET /product/:product_id
router.get('/:product_id', function (req, res) {
    req.app.get('models').Product
        .find({
            where: {
                id: req.params.product_id
            },
            include: [req.app.get('models').ProductPhoto]
        })
        .then(function (product) {
            res.status(200).json(product);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
});

// DELETE /product/:product_id
router.delete('/:product_id', function (req, res) {
    var Product = req.app.get('models').Product;
    Product.find({
            where: {
                id: req.params.product_id
            },
            include: [req.app.get('models').ProductPhoto]
        })
        .then(function (product) {
        logger.warn('destroying product');
            product.ProductPhotos.forEach(function (photo) {
                try {
                    fs.unlinkSync(path.join(__dirname, '../', photo.path));
                } catch(e) {}
            });
            return product.destroy();
        })
        .then(function () {
            res.sendStatus(204);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
});

// GET /product/:user_id/suggest|?radius=40|
router.get('/:user_id/suggest', function (req, res) {
 var queryString1 = 'SELECT Products.price+Products.supplement as value FROM Products where Products.user_id = :user_id group by value';
        
var db = req.app.get('models');

return db.sequelize.query(queryString1, {
                type: db.sequelize.QueryTypes.SELECT,
                replacements: {
                    user_id: req.params.user_id
                }
            })
        .then(function (values) {
        
        var str = 'AND ( ';
for (var i = 0; i < values.length; i++) {
	str += '(Products.price+Products.supplement BETWEEN ' + values[i].value*0.8 +' AND ' +values[i].value*1.2 +')';
	if(i<values.length-1){
	
	str+= ' OR ';
	}
}
str+=')';

            var queryString = 'SELECT Products.*, Photos.path, ' +
        '(((acos(sin(( :latitude *pi()/180)) * sin((`lat`*pi()/180))+cos(( :latitude *pi()/180)) * cos((`lat`*pi()/180)) * cos((( :longitude  - `lon`)* pi()/180))))*180/pi())*60*1.1515*1.609344 ) ' +
        'AS distance ' +
        'FROM `Users` JOIN `Products` on `Users`.id = Products.user_id ' +
        'LEFT JOIN ( ' +
        ' SELECT DISTINCT product_id, path FROM ProductPhotos GROUP BY product_id ) AS Photos ' +
        'on Photos.product_id = Products.id ' +
        'WHERE :user_id != Products.user_id ' +
        'AND Products.id NOT  IN (select liked_product_id from Likes where Likes.user_id = :user_id) ' +
        str +
        'HAVING distance <= :radius LIMIT 10';

    var db = req.app.get('models');
    db.User.find(req.params.user_id)
        .then(function (user) {
            if ( !user.lat || !user.lon ) {
                throw "No user location data available.";
            }
            return db.sequelize.query(queryString, {
                type: db.sequelize.QueryTypes.SELECT,
                replacements: {
                    latitude: user.lat,
                    longitude: user.lon,
                    user_id: user.id,
                    radius: req.query.radius || 50
                }
            })
        })
        .then(function (productsWithDistance) {
            res.status(200).json(productsWithDistance);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
        })
        .catch(function (err) {
            response.status(400).json(err);
        });
        
   
});

// GET /product/:product_id/suggestions|?radius=40&priceOffset=10|
router.get('/:user_id/suggestions', function (req, res) {
    var queryString = 'SELECT Products.*, Photos.path, ' +
        '(((acos(sin(( :latitude *pi()/180)) * sin((`lat`*pi()/180))+cos(( :latitude *pi()/180)) * cos((`lat`*pi()/180)) * cos((( :longitude  - `lon`)* pi()/180))))*180/pi())*60*1.1515*1.609344 ) ' +
        'AS distance ' +
        'FROM `Users` JOIN `Products` on `Users`.id = Products.user_id ' +
        'LEFT JOIN ( ' +
        ' SELECT DISTINCT product_id, path FROM ProductPhotos GROUP BY product_id ) AS Photos ' +
        'on Photos.product_id = Products.id ' +
        'WHERE :user_id != Products.user_id ' +
        'AND Products.id NOT  IN (select product_id_target from Likes where Likes.product_id_source = :product_id) ' +
        'AND Products.price+Products.supplement BETWEEN :lowest_price AND :highest_price ' +
        'HAVING distance <= :radius ';



    var db = req.app.get('models');

    db.Product.find({
            where: {
                id: req.params.product_id
            },
            include: [db.User]
        })
        .then(function (product) {
            req.query.priceOffset = +req.query.priceOffset;
            req.query.radius = +req.query.radius;
            if ( !product ) {
                throw "Product not found";
            }
            if ( !product.User.lat || !product.User.lon ) {
                throw "No user location data available.";
            }
            if ( isNaN(req.query.radius) ) {
                req.query.radius = 0;
            }
            if ( isNaN(req.query.priceOffset) ) {
                req.query.priceOffset = 0;
            }
            logger.info('The following product was found: ' + JSON.stringify(product));
            var totalPrice = product.price + (product.supplement || 0);
            var priceOffset =  req.query.priceOffset ? req.query.priceOffset < 100 ? req.query.priceOffset : 100 : 20 ;
            var priceDifference = totalPrice * ( priceOffset / 100 );
            return db.sequelize.query(queryString, {
                type: db.sequelize.QueryTypes.SELECT,
                replacements: {
                    latitude: product.User.lat,
                    longitude: product.User.lon,
                    user_id: product.user_id,
                    product_id: product.id,
                    lowest_price: product.price - priceDifference,
                    highest_price: product.price + priceDifference,
                    radius: (+req.query.radius || 50)
                }
            });
        })
        .then(function (productsWithDistance) {
            logger.info('The following products were retrieved: ' + JSON.stringify(productsWithDistance));
            res.status(200).json(productsWithDistance);
        })
        .catch(function (err) {
            logger.error('Error while trying to suggest products: ' + JSON.stringify(err));
            res.status(400).json(err);
        });
});

router.use('/:user_id/likes', passProductParams);
router.use('/:user_id/likes', require('./likes'));
router.use('/:product_id/chat', passProductParams);
router.use('/:product_id/chat', require('./chat'));

module.exports = router;
