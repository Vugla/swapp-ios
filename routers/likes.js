var express = require('express');
var logger = require('winston');
var _ = require('lodash');
var pushManager = require('./../utils/iosPushManager');
var router = express.Router();


// GET /product/:product_id/likes|&type=count|
router.get('/', function (req, res) {
    var Likes = req.app.get('models').Likes;
    Likes.findAndCountAll({
            where: {
                product_id_target: req.productParams.product_id,
                disliked: false
            }
        })
        .then(function (result) {
            if ('type' in req.query) {
                if (req.query.type === 'count') {
                    res.status(200).json({
                        count: result.count || 0
                    });
                }
            } else {
                req.app.get('models').Product
                    .findAll({
                        where: {
                            id: _.map(result.rows, 'product_id_source')
                        }
                    })
                    .then(function (products) {
                        res.status(200).json(products);
                    })
                    .catch(function (err) {
                        res.status(400).json(err);
                    });

            }
        })
        .catch(function (err) {
            logger.error('Error retrieving likes for product ' + req.productParams.product_id);
            res.status(400).json(err);
        });
});


router.get('/matches', function (req, res) {

     var queryString = 'SELECT Likes.*, photo_source.path as source_path, photo_target.path as target_path FROM `Likes` '+
    'LEFT JOIN (SELECT product_id, path FROM ProductPhotos GROUP BY product_id) as photo_source on photo_source.product_id = Likes.product_id_source '+
    'LEFT JOIN (SELECT product_id, path FROM ProductPhotos GROUP BY product_id) as photo_target on photo_target.product_id = Likes.product_id_target '+
    'WHERE product_id_target = :product_id '+
    'AND product_id_source IN ( '+
        'SELECT product_id_target from Likes '+
        'where product_id_source = :product_id '+
        'AND disliked IS NOT TRUE)  AND Likes.disliked IS NOT TRUE';

    var db = req.app.get('models');
    db.sequelize.query(queryString, {
            type: db.sequelize.QueryTypes.SELECT,
            replacements: {
                product_id: req.productParams.product_id
            }
        })
        .then(function (matches) {
            res.status(200).json(matches);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
});

// POST /product/:user_id/likes/:another_product_id
router.route('/:target_product_id')
    .post(function (req, res) {
       
        var options = {
            user_id: req.productParams.user_id,
            product_id_target: req.params.target_product_id,
            disliked: req.body.disliked === 'true' || req.body.disliked === true || req.body.disliked === 1
        };
        
        req.app.get('models').Likes.findOrCreate({
                        where: {
                            user_id: options.user_id,
                            liked_product_id: options.product_id_target
                        }
                    })
                    .spread(function (like, created, Users) {
                        if ( !like.disliked != !options.disliked ) {
                            like.updateAttributes({
                                disliked: options.disliked
                            });
                        }
                        
                        var db = req.app.get('models');
                         
                          var queryString = 'SELECT * FROM (SELECT source_info.*,product_photos_source_product.path '+
                        'as source_product_path FROM (select Products.user_id AS source_user_id,'+
                        ' Products.id AS source_product_id, Products.name AS source_product_name, '+
                        'Products.description AS source_product_description, Products.price AS source_product_price, '+
                        'Products.supplement AS source_product_supplement, Products.isMoney AS source_product_isMoney '+
                        'FROM (SELECT * FROM Likes WHERE Likes.user_id=(SELECT Products.user_id FROM Products WHERE'+
                        ' Products.id=:target_product_id) AND Likes.disliked IS NOT TRUE AND Likes.liked_product_id IN'+
                        ' (SELECT Products.id FROM Products WHERE Products.user_id=:user_id))AS my_liked_products JOIN '+
                        'Products ON my_liked_products.liked_product_id=Products.id WHERE (Products.price+Products.supplement'+
                        ' BETWEEN 0.8*(SELECT Products.price+Products.supplement FROM Products WHERE '+
                        'Products.id=:target_product_id) AND 1.2*(SELECT Products.price+Products.supplement FROM'+
                        ' Products WHERE Products.id=:target_product_id)) OR ((SELECT Products.price+Products.supplement FROM'+
                        ' Products WHERE Products.id=:target_product_id) BETWEEN 0.8*(Products.price+Products.supplement) AND'+
                        ' 1.2*(Products.price+Products.supplement))) AS source_info LEFT JOIN(SELECT * FROM ProductPhotos'+
                        ' GROUP BY ProductPhotos.product_id) AS product_photos_source_product ON '+
                        'product_photos_source_product.product_id = source_info.source_product_id )AS source_info_total JOIN'+
                        ' (SELECT target_info.*, product_photos_target_product.path AS target_product_path FROM '+
                        '(SELECT Products.user_id AS target_user_id, Products.id AS target_product_id, Products.name AS target_product_name,'+
                        ' Products.description AS target_product_description, Products.price AS target_product_price,'+
                        ' Products.supplement AS target_product_supplement, Products.isMoney AS target_product_isMoney FROM'+
                        ' Products WHERE Products.id=:target_product_id) AS target_info LEFT JOIN '+
                        '(SELECT * FROM ProductPhotos GROUP BY ProductPhotos.product_id) AS product_photos_target_product ON'+
                        ' product_photos_target_product.product_id = target_info.target_product_id) AS target_info_total'                       
    
// SELECT * FROM (SELECT source_info.*,product_photos_source_product.path as source_product_path FROM (select Products.user_id as source_user_id, Products.id as source_product_id, Products.name as source_product_name, Products.description as source_product_description, Products.price as source_product_price, Products.supplement as source_product_supplement, Products.isMoney as source_product_isMoney from (select * from Likes where Likes.user_id=(select Products.user_id from Products where Products.id='2422fa30-cefc-11e4-a0cf-051262824ae8') and Likes.disliked is not true and Likes.liked_product_id IN (select Products.id from Products where Products.user_id='3cd12bbd-6394-4579-aefa-1f87d26a956d'))as my_liked_products JOIN Products on my_liked_products.liked_product_id=Products.id where (Products.price+Products.supplement BETWEEN 0.8*(SELECT Products.price+Products.supplement from Products where Products.id='2422fa30-cefc-11e4-a0cf-051262824ae8') AND 1.2*(SELECT Products.price+Products.supplement from Products where Products.id='2422fa30-cefc-11e4-a0cf-051262824ae8')) OR ((SELECT Products.price+Products.supplement from Products where Products.id='2422fa30-cefc-11e4-a0cf-051262824ae8') BETWEEN 0.8*(Products.price+Products.supplement) AND 1.2*(Products.price+Products.supplement))) as source_info LEFT JOIN(select * from ProductPhotos GROUP BY ProductPhotos.product_id) as product_photos_source_product ON product_photos_source_product.product_id = source_info.source_product_id )AS source_info_total JOIN (SELECT target_info.*, product_photos_target_product.path AS target_product_path FROM (SELECT Products.user_id as target_user_id, Products.id as target_product_id, Products.name as target_product_name, Products.description as target_product_description, Products.price as target_product_price, Products.supplement as target_product_supplement, Products.isMoney as target_product_isMoney FROM Products WHERE Products.id='2422fa30-cefc-11e4-a0cf-051262824ae8') as target_info LEFT JOIN(select * from ProductPhotos GROUP BY ProductPhotos.product_id) as product_photos_target_product ON product_photos_target_product.product_id = target_info.target_product_id) AS target_info_total                       
                             return db.sequelize.query(queryString, {
                type: db.sequelize.QueryTypes.SELECT,
                replacements: {
                    target_product_id: options.product_id_target,
                    user_id: options.user_id
                }
            });

                    })
                    .then(function (likes) {
                        var result = {
                            match: likes.length>0 && !options.disliked
                        };
                        console.log(JSON.stringify(likes));
                        console.log(JSON.stringify(options));
                       
                        var db = req.app.get('models');
                        var participant = db.User.find({
            where: {
                id: options.user_id
            }
        })
                        if (result.match && !options.disliked && ( participant.notify_match === 'true' || participant.notify_match === true) ) {
                            pushManager.notifyUser(participant, 'You got a match!', {
                                product_id: options.product_id_target,
                                notify_match: options.notify_match
                            });
                        }
                        res.status(201).json(likes);
                    })
                    .catch(function (err) {
                        logger.error('Error on like: ' + err);
                        res.status(400).json(err);
                    });
            });
   //  })
   
    // DELETE /product/:product_id/likes/:another_product_id
   //  .delete(function (req, res) {
//         var Likes = req.app.get('models').Likes;
//         Likes.destroy({
//                 where: {
//                     product_id_source: req.productParams.product_id,
//                     product_id_target: req.params.target_product_id
//                 }
//             })
//             .then(function () {
//                 res.sendStatus(204);
//             })
//             .catch(function (err) {
//                 res.status(400).json(err);
//             });
//     });

module.exports = router;
