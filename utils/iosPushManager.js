var config = require('../config/config.json');
var apn = require('apn');
var logger = require('winston');
var connectionOptions = {
    cert: './certs/' + config[process.env.NODE_ENV].apn_cert,
    key: './certs/' + config.apn_key,
    production: process.env.NODE_ENV === 'production'
};


function notifyUser(user, message, payload) {
    if( !user.device_id || user.device_id == undefined || user.device_id === 'undefined' || user.device_id === 'null' ) {
        return;
    }
    var apnConnection = new apn.Connection(connectionOptions);
    var device = new apn.Device(user.device_id);
    var note = new apn.Notification();

    logger.info('Sending notification to user ' + user.name);

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.sound = "ping.aiff";
    note.alert = ( message || "You have a new notification" );
    note.payload = payload || {};

    apnConnection.pushNotification(note, device);
}


module.exports = {
    notifyUser: notifyUser
};
