"use strict";

var fs        = require("fs");
var path      = require("path");
var basename  = path.basename(module.filename);
var utils = {};

fs
  .readdirSync(__dirname)
  .filter(function filterFile(file) {
    return (file.indexOf(".") !== 0) && (file !== basename);
  })
  .forEach(function iterateFile(file) {
    var utilName = file.slice(0, file.lastIndexOf('.'));
    var util = require(path.join(__dirname, file));
    utils[utilName] = util;
  });

module.exports = utils;
