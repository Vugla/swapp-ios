var request = require('request');
var url = require('url');
var logger = require('winston');
var RSVP = require('rsvp');
var config    = require('../config/config.json');

function makeRequest(options) {
    logger.warn('Fb graph URL: ' + url.format(options));
    return new RSVP.Promise(function (resolve, reject) {

        request(url.format(options), function (err, response, body) {
            logger.warn('Fb graph API reached. ' + body );
            body = body && JSON.parse(body);
            var fbError = (body && 'data' in body && typeof body.data.error !== "undefined") ? body.data.error : undefined;

//            logger.debug('Fb graph API reached. ' + JSON.stringify(body) );
            if (!err && !fbError && response.statusCode == 200 ) {
                logger.info(module.filename + "::Successfully made request:");
                logger.info(module.filename + "::Response:" + JSON.stringify(body) );
                resolve(body.data || body);
            }
            if (!err && response.statusCode != 200) {
                logger.error(module.filename + "::Bad status code from FB Graph api:" + response.statusCode);
                reject({
                    status: response.statusCode,
                    info: response.statusText
                });
            }
            if (err || fbError) {
                logger.error(module.filename + "::Error reaching FB Graph api for token: " + options.query.access_token );
                logger.error(module.filename + "::Error:" + err);
                reject({
                    status: -1,
                    info: err || fbError.message
                });
            }
        });

    });
}

function verifyToken(token) {
    var options = {
        protocol: 'https',
        hostname: 'graph.facebook.com',
        pathname: '/debug_token',
        query: {
            input_token: token,
            access_token: config.fb_token
        }
    };

    return makeRequest(options, token);
}

function getUser(token) {
    var options = {
        protocol: 'https',
        hostname: 'graph.facebook.com',
        pathname: '/me',
        query: {
            fields: 'id, email, name, verified',
            access_token: token
        }
    };

    return makeRequest(options, token);
}

function buildUserPhotoUrl(fbUserId) {
    var fbUrl = 'http://graph.facebook.com/' + fbUserId + '/picture?type=large';
    return fbUrl;
}

module.exports = {
    verifyToken: verifyToken,
    getUser: getUser,
    buildUserPhotoUrl: buildUserPhotoUrl
};
