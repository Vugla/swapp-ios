"use strict";
module.exports = function (sequelize, DataTypes) {
    var Chat = sequelize.define("Chat", {
        product_id: DataTypes.UUID,
        participant_product_id: DataTypes.UUID,
        message: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.belongsTo(models.Product, {
                    foreignKey: "product_id",
                    onDelete: 'cascade'
                });
            }
        }
    });
    return Chat;
};
