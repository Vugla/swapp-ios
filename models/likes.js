"use strict";
module.exports = function (sequelize, DataTypes) {
    var Likes = sequelize.define("Likes", {
        liked_product_id: DataTypes.UUID,
        user_id: DataTypes.UUID,
        disliked: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.belongsTo(models.Product, { foreignKey: "liked_product_id", onDelete: 'cascade' });
                 this.belongsTo(models.User, { foreignKey: "user_id", onDelete: 'cascade' });
            }
        }
    });

    return Likes;
};
