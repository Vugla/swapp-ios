"use strict";
module.exports = function (sequelize, DataTypes) {
    var Product = sequelize.define("Product", {
        id: {
            allowNull: false,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true,
        },
        name: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
        description: DataTypes.TEXT,
        price: {
            allowNull: false,
            type: DataTypes.DECIMAL,
            validate: {
                min: 0,
                notEmpty: true
            }
        },
        supplement: {
            allowNull: false,
            type: DataTypes.DECIMAL(10, 2)
        },
        user_id: {
            allowNull: false,
            type: DataTypes.UUID,
            validate: {
                notEmpty: true
            }
        },
        ends_at: {
            type: DataTypes.DATE,
            validate: {
                isAfter: new Date()
            }
        },
        isMoney: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.hasMany(models.Likes, {
                    foreignKey: 'liked_product_id',
                    onDelete: 'cascade',
                    hooks: true
                });
                this.hasMany(models.ProductPhoto, {
                    foreignKey: 'product_id',
                    onDelete: 'cascade',
                    hooks: true
                })
                this.hasMany(models.Chat, {
                    foreignKey: 'product_id',
                    onDelete: 'cascade',
                    hooks: true
                });
                this.belongsTo(models.User, {
                    foreignKey: "user_id",
                    onDelete: 'cascade'
                });
            }
        },
        instanceMethods: {
            countLikes: function () {
                return this.__factory.associations['Likes'].target.count({
                    where: {
                        liked_product_id: this.id
                    }
                });
            }
        }
    });
    return Product;
};
