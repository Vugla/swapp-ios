"use strict";
var logger = require('winston');
module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define("User", {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false
        },
        name: DataTypes.STRING,
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            set: function (email) {
                this.setDataValue('email', email.toLowerCase());
            }
        },
        notify_chat: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        notify_match: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        fb_id: DataTypes.BIGINT,
        device_id: DataTypes.TEXT,
        lon: {
            type: DataTypes.DECIMAL(11, 8),
            validate: {
                min: -180.000000,
                max: 180.000000
            }
        },
        lat: {
            type: DataTypes.DECIMAL(10, 8),
            validate: {
                min: -90.000000,
                max: 90.000000
            }
        },
        photo: DataTypes.STRING,
        fb_token: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.hasMany(models.Product, {
                    foreignKey: 'user_id',
                    onDelete: 'cascade',
                    hooks: true
                });
                this.hasMany(models.Likes, {
                    foreignKey: 'user_id',
                    onDelete: 'cascade',
                    hooks: true
                });
            }
        },
        validate: {
            bothCoordsOrNone: function () {
                logger.warn('Arguments' + JSON.stringify(this));
                if (!!this.lat !== !!this.lon) {
                    throw new Error('Require either both latitude and longitude or neither');
                }
            }
        }
    });
    return User;
};
