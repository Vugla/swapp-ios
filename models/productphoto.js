"use strict";
var fs = require('fs');
var path = require('path');
var logger = require('winston');
module.exports = function (sequelize, DataTypes) {
    var ProductPhoto = sequelize.define("ProductPhoto", {
        product_id: DataTypes.UUID,
        path: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                this.belongsTo(models.Product, {
                    foreignKey: "product_id",
                    onDelete: 'cascade'
                });
            }
        },
        hooks: {
            bofreDestroy: function(photo, options, fn) {
                logger.warn('!!!hook called!!!!!!!!!!!!!');
                fs.unlinkSync(path.join(__dirname, '../', photo.path));
                return fn();
            },
            beforeBulkDestroy: function(photos, options, fn) {
                logger.warn('hook called!!!!!!!!!!!!!');
                photos.forEach(function (photo) {
                    fs.unlinkSync(path.join(__dirname, '../', photo.path));
                });
                return fn();
            }
        }
    });
    return ProductPhoto;
};
